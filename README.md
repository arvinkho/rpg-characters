# Background
This is a console Java application for the first assignment in the Accelerate course.

It covers fundamental OOP concepts, such as inheritance, encapsulation, polymorphism and unit testing.

# Install
* Install JDK 17
* Install IntelliJ
* Clone repo

# Usage
This application is not implemented into anything useful - YET! This project can be used as a simple library and implemented into i.e. a command line RPG.
