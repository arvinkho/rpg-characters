package no.noroff.accelerate.exceptions;


/**
 * Exception thrown when trying to equip an invalid weapon item,
 * such as the character level is too low or weapon type is incorrect.
 */
public class InvalidWeaponException extends Exception{

    public InvalidWeaponException(String message) {
        super(message);
    }
}
