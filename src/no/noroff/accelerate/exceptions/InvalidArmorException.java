package no.noroff.accelerate.exceptions;


/**
 * Exception thrown when trying to equip an invalid armor item,
 * such as the character level is too low or armor type is incorrect.
 */
public class InvalidArmorException extends Exception{

    public InvalidArmorException(String message) {
        super(message);
    }
}
