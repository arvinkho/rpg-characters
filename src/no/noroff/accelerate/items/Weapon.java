package no.noroff.accelerate.items;

/**
 * Represents a weapon. This is an extension of the Item class.
 *
 * @author arvinkho
 */
public class Weapon extends Item{

    private final WeaponType WEAPON_TYPE;
    private final double dps;

    /**
     * Creates an instance of weapon
     * @param name name of the weapon
     * @param requiredLevel the required level to equip the weapon
     * @param weaponType the type of weapon
     * @param damage the damage per hit of the weapon
     * @param attackSpeed how many times per second this weapon can attack
     */
    public Weapon(String name, int requiredLevel, WeaponType weaponType, double damage, double attackSpeed){
        super(name, requiredLevel, Slot.WEAPON);
        this.WEAPON_TYPE = weaponType;
        this.dps = damage * attackSpeed;
    }

    /**
     * Returns the dps of the weapon
     * @return the dps of the weapon
     */
    public double getDps() {
        return this.dps;
    }

    /**
     * Returns the type of weapon
     * @return the type of weapon
     */
    public WeaponType getWeaponType() {
        return this.WEAPON_TYPE;
    }

}
