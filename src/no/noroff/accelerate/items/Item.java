package no.noroff.accelerate.items;


/**
 * Represents an item. Contains the common information about an item,
 * such as item name, required level and appropriate slot.
 *
 * @author arvinkho
 */
public abstract class Item {

    private final String itemName;
    private final int requiredLevel;
    private final Slot SLOT;

    /**
     * Creates an instance of an item
     * @param itemName name of the item
     * @param requiredLevel required level to equip the item
     * @param slot appropriate slot
     */
    public Item(String itemName, int requiredLevel, Slot slot){
        this.itemName = itemName;
        this.requiredLevel = requiredLevel;
        this.SLOT = slot;
    }

    /**
     * Returns the required level to equip the item
     * @return the required level to equip the item
     */
    public int getRequiredLevel() {
        return requiredLevel;
    }

    /**
     * Returns the appropriate slot to equip the item in
     * @return the appropriate slot to equip the item in
     */
    public Slot getSlot() {
        return this.SLOT;
    }

    /**
     * Returns the name of the item
     * @return the name of the item
     */
    public String getItemName() {
        return itemName;
    }
}
