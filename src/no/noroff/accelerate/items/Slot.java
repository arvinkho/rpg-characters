package no.noroff.accelerate.items;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
