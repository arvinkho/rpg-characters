package no.noroff.accelerate.items;

import no.noroff.accelerate.characters.PrimaryAttribute;

/**
 * Represents an armor. This is an extension of the Item class.
 *
 * @author arvinkho
 */
public class Armor extends Item{

    private final ArmorType ARMOR_TYPE;
    private final PrimaryAttribute ATTRIBUTES;

    /**
     * Creates an instance of armor
     * @param name the name of the armor
     * @param requiredLevel the required level to equip the armor
     * @param armorType the type of armor
     * @param slot the appropriate slot for the armor
     * @param attribute the bonus attributes the armor provides
     */
    public Armor(String name, int requiredLevel, ArmorType armorType, Slot slot, PrimaryAttribute attribute){
        super(name, requiredLevel, slot);
        this.ARMOR_TYPE = armorType;
        this.ATTRIBUTES = attribute;
    }

    /**
     * Returns the type of armor
     * @return the type of armor
     */
    public ArmorType getArmorType() {
        return this.ARMOR_TYPE;
    }

    /**
     * Returns the bonus attributes provided by the armor
     * @return the bonus attributes provided by the armor
     */
    public PrimaryAttribute getAttributes(){
        return this.ATTRIBUTES;
    }

}
