package no.noroff.accelerate.items;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
