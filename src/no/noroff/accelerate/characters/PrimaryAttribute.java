package no.noroff.accelerate.characters;


/**
 * Represents three primary attributes:
 * Strength, dexterity and intelligence.
 * @author arvinkho
 */
public class PrimaryAttribute {

    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     * Creates an instance of PrimaryAttribute
     * @param strength initial strength level
     * @param dexterity initial dexterity level
     * @param intelligence initial intelligence level
     */
    public PrimaryAttribute (int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Increases the strength by a given value
     * @param incVal value to increase with
     */
    public void increaseStrength(int incVal){
        this.strength+=incVal;
    }

    /**
     * Increases the dexterity by a given value
     * @param incVal value to increase with
     */
    public void increaseDexterity(int incVal){
        this.dexterity+=incVal;
    }

    /**
     * Increases the intelligence by a given value
     * @param incVal value to increase with
     */
    public void increaseIntelligence(int incVal){
        this.intelligence+=incVal;
    }

    /**
     * Returns the current strength level
     * @return the current strength level
     */
    public int getStrength() {
        return this.strength;
    }

    /**
     * Returns the current dexterity level
     * @return the current dexterity level
     */
    public int getDexterity() {
        return this.dexterity;
    }

    /**
     * Returns the current intelligence level
     * @return the current intelligence level
     */
    public int getIntelligence() {
        return this.intelligence;
    }

    /**
     * Adds attributes from another PrimaryAttribute object
     * to this current instance.
     * @param attribute PrimaryAttribute object to add
     */
    public void addAttributes(PrimaryAttribute attribute) {
        this.strength += attribute.getStrength();
        this.dexterity += attribute.getDexterity();
        this.intelligence += attribute.getIntelligence();
    }
}
