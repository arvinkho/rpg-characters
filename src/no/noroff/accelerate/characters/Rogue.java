package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.WeaponType;

/**
 * Represents a Rogue. This is an extension of the character class.
 *
 * @author arvinkho
 */
public class Rogue extends Character{

    private static final WeaponType[] WEAPON_TYPES = {WeaponType.DAGGER, WeaponType.SWORD};
    private static final ArmorType[] ARMOR_TYPES = {ArmorType.LEATHER, ArmorType.MAIL};
    private static final int START_STRENGTH = 2;
    private static final int START_DEXTERITY = 6;
    private static final int START_INTELLIGENCE = 1;
    private static final int LEVELUP_STRENGTH = 1;
    private static final int LEVELUP_DEXTERITY = 4;
    private static final int LEVELUP_INTELLIGENCE = 1;

    /**
     * Creates an instance of a rogue
     * @param name name of the rogue
     */
    public Rogue(String name){
        super(name, START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE, WEAPON_TYPES, ARMOR_TYPES);
    }

    /**
     * Levels up the rogue. Increases the level by one point, and supplies with
     * rogue specific values for increasing the base attributes.
     */
    public void levelUp(){
        super.levelUp(LEVELUP_STRENGTH, LEVELUP_DEXTERITY, LEVELUP_INTELLIGENCE);
    }


    /**
     * Calculates the total dps based on weapon and primary attribute (dexterity).
     */
    public void calculateDps() {
        super.calculateDps(this.getDexterity());
    }
}
