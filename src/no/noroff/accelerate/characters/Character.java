package no.noroff.accelerate.characters;


import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.items.*;

import java.util.HashMap;

/**
 * Represents a character. Contains the common information about
 * a character, such as base attributes, total attributes (base+items),
 * current level, name, allowable weapon- and armor types, equipped items
 * and damage per second (DPS).
 * @author arvinkho
 */
public abstract class Character{


    //TODO ADD PRIMARY ATTRIBUTE AS AN ENUM?
    protected final PrimaryAttribute baseAttributes;
    protected PrimaryAttribute totalAttributes;
    protected int level;
    protected final String name;
    protected final WeaponType[] WEAPON_TYPES;
    protected final ArmorType[] ARMOR_TYPES;
    protected final HashMap<Slot, Item> equippedItems = new HashMap<>();
    protected double dps;

    /**
     * Creates an instance of a character
     *
     * @param name                  Character name
     * @param startStrength         Base strength at level 1
     * @param startDexterity        Base dexterity at level 1
     * @param startIntelligence     Base intelligence at level 1
     * @param weaponTypes           Allowable weapon types
     * @param armorTypes            Allowable armor types
     */
    public Character(String name,
                     int startStrength,
                     int startDexterity,
                     int startIntelligence,
                     WeaponType[] weaponTypes,
                     ArmorType[] armorTypes){
        this.name = name;
        this.level = 1;
        this.baseAttributes = new PrimaryAttribute(startStrength, startDexterity, startIntelligence);
        this.WEAPON_TYPES = weaponTypes;
        this.ARMOR_TYPES = armorTypes;
        calculateTotalAttributes();
    }

    /**
     * Increases the level by one point, as well as updating the base stats.
     * @param strengthInc       the strength increase per level
     * @param dexterityInc      the dexterity increase per level
     * @param intelligenceInc   the intelligence increase per level
     */
    public void levelUp(int strengthInc, int dexterityInc, int intelligenceInc){
        baseAttributes.increaseStrength(strengthInc);
        baseAttributes.increaseDexterity(dexterityInc);
        baseAttributes.increaseIntelligence(intelligenceInc);
        this.level++;
    }

    /**
     * Updates the total attributes. Checks if any armor is equipped,
     * and updates the total attributes with the current equipped armor
     * and base attributes
     */
    public void calculateTotalAttributes(){
        this.totalAttributes = new PrimaryAttribute(0, 0, 0);
        this.totalAttributes.addAttributes(this.baseAttributes);

        if (equippedItems.containsKey(Slot.BODY)){
            this.totalAttributes.addAttributes(((Armor) getItem(Slot.BODY)).getAttributes());
        }

        if (equippedItems.containsKey(Slot.HEAD)){
            this.totalAttributes.addAttributes(((Armor) getItem(Slot.HEAD)).getAttributes());
        }

        if (equippedItems.containsKey(Slot.LEGS)){
            this.totalAttributes.addAttributes(((Armor) getItem(Slot.LEGS)).getAttributes());
        }
    }

    /**
     * Calculates the current dps based on the primary attribute value
     * as well as the currently equipped weapon dps. If no weapon is equipped,
     * the dps is 1.
     * @param primaryAttributeValue primary attribute value
     */
    public void calculateDps(double primaryAttributeValue) { //TODO IF PRIMARY ATTRIBUTE IS ENUM, MAKE PRIVATE AND CALL FROM getDps
        double weaponDps = 1.0;
        if (equippedItems.containsKey(Slot.WEAPON)){
            weaponDps = this.getWeapon().getDps();
        }
        this.dps = weaponDps * (1.0 + primaryAttributeValue/100);
    }

    /**
     * Equips a weapon in the weapon slot. Assures that the weapon is valid
     * (correct type and appropriate level).
     * @param weapon weapon to equip
     * @return true if successful
     * @throws InvalidWeaponException if the weapon level is too high or the weapon type is not supported
     */
    public boolean equipItem(Weapon weapon) throws InvalidWeaponException{
        if (weapon.getRequiredLevel() > this.level) {
            throw new InvalidWeaponException("Insufficient level!");
        }
        for (WeaponType weaponType : this.WEAPON_TYPES) {
            if (weapon.getWeaponType() == weaponType) {
                equippedItems.put(Slot.WEAPON, weapon);
                return true;
            }
        }
        throw new InvalidWeaponException("You can't equip this weapon type!");
    }


    /**
     * Equips armor in the appropriate slot. Check which slot it fits in,
     * and assures the armor is valid (correct type and appropriate level).
     * @param armor armor to equip
     * @return true if successful
     * @throws InvalidArmorException if the armor level is too high or the armor type is not supported
     */
    public boolean equipItem(Armor armor) throws InvalidArmorException{
        Slot slot = armor.getSlot();
        if (armor.getRequiredLevel() > this.level) {
            throw new InvalidArmorException("Insufficient level!");
        }
        for (ArmorType armorType : this.ARMOR_TYPES) {
            if (armor.getArmorType() == armorType) {
                equippedItems.put(slot, armor);
                calculateTotalAttributes();
                return true;
            }
        }
        throw new InvalidArmorException("You can't equip this armor type!");
    }

    /**
     * If an item is currently equipped in the given slot, this item
     * is removed.
     * @param slot slot to remove item from
     */
    public void unequipItem(Slot slot) {
        this.equippedItems.remove(slot);
    }

    /**
     * Returns the total strength level.
     * @return the total strength level
     */
    public int getStrength(){
        calculateTotalAttributes();
        return totalAttributes.getStrength();
    }

    /**
     * Returns the total dexterity level
     * @return the total dexterity level
     */
    public int getDexterity(){
        calculateTotalAttributes();
        return totalAttributes.getDexterity();
    }

    /**
     * Returns the total intelligence level
     * @return the total intelligence level
     */
    public int getIntelligence(){
        calculateTotalAttributes();
        return totalAttributes.getIntelligence();
    }

    /**
     * Returns the current level
     * @return the current level
     */
    public int getLevel(){
        return this.level;
    }

    /**
     * Returns the currently equipped weapon
     * @return the currently equipped weapon
     */
    public Weapon getWeapon() {
        return (Weapon) this.equippedItems.get(Slot.WEAPON);
    }

    /**
     * Returns the currently equipped item in the given item slot
     * @param slot slot to get item from
     * @return the current item in the selected slot
     */
    public Item getItem(Slot slot) {
        //ADD ERROR HANDLING, MAYBE CHECK IF NOT WEAPON?
        return this.equippedItems.get(slot);
    }

    /**
     * Returns the current DPS
     * @return the current DPS
     */
    public double getDps(){
        return this.dps;
    }

    /**
     * Returns the base attributes (without gear bonus stats)
     * @return the base attributes
     */
    public PrimaryAttribute getBaseAttributes() {
        return this.baseAttributes;
    }

    /**
     * Returns the character name
     * @return the character name
     */
    public String getName() {
        return name;
    }

    /**
     * Builds a string with name, level, total attributes and total dps and returns it
     * @return a string with stats
     */
    public String getStats() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name               : ");
        builder.append(this.getName());
        builder.append("\nLevel              : ");
        builder.append(this.getLevel());
        builder.append("\nTotal Strength     : ");
        builder.append(this.getStrength());
        builder.append("\nTotal Dexterity    : ");
        builder.append(this.getDexterity());
        builder.append("\nTotal Intelligence : ");
        builder.append(this.getIntelligence());
        builder.append("\nTotal DPS          : ");
        builder.append(this.getDps());
        return builder.toString();
    }
}
