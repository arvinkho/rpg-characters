package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.WeaponType;

/**
 * Represents a Warrior. This is an extension of the character class.
 *
 * @author arvinkho
 */
public class Warrior extends Character{

    private static final WeaponType[] WEAPON_TYPES = {WeaponType.AXE, WeaponType.HAMMER,
    WeaponType.SWORD};
    private static final ArmorType[] ARMOR_TYPES = {ArmorType.MAIL, ArmorType.PLATE};
    private static final int START_STRENGTH = 5;
    private static final int START_DEXTERITY = 2;
    private static final int START_INTELLIGENCE = 1;
    private static final int LEVELUP_STRENGTH = 3;
    private static final int LEVELUP_DEXTERITY = 2;
    private static final int LEVELUP_INTELLIGENCE = 1;

    /**
     * Creates an instance of a warrior
     * @param name name of the warrior
     */
    public Warrior(String name){
        super(name, START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE, WEAPON_TYPES, ARMOR_TYPES);
    }

    /**
     * Levels up the warrior. Increases the level by one point, and supplies with
     * warrior specific values for increasing the base attributes.
     */
    public void levelUp(){
        super.levelUp(LEVELUP_STRENGTH, LEVELUP_DEXTERITY, LEVELUP_INTELLIGENCE);
    }


    /**
     * Calculates the total dps based on weapon and primary attribute (strength).
     */
    public void calculateDps() {
        super.calculateDps(this.getStrength());
    }
}
