package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.WeaponType;

/**
 * Represents a Ranger. This is an extension of the character class.
 *
 * @author arvinkho
 */
public class Ranger extends Character{

    private static final WeaponType[] WEAPON_TYPES = {WeaponType.BOW};
    private static final ArmorType[] ARMOR_TYPES = {ArmorType.LEATHER, ArmorType.MAIL};
    private static final int START_STRENGTH = 1;
    private static final int START_DEXTERITY = 7;
    private static final int START_INTELLIGENCE = 1;
    private static final int LEVELUP_STRENGTH = 1;
    private static final int LEVELUP_DEXTERITY = 5;
    private static final int LEVELUP_INTELLIGENCE = 1;

    /**
     * Creates an instance of a ranger
     * @param name name of the ranger
     */
    public Ranger(String name){
        super(name, START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE, WEAPON_TYPES, ARMOR_TYPES);
    }

    /**
     * Levels up the ranger. Increases the level by one point, and supplies with
     * ranger specific values for increasing the base attributes.
     */
    public void levelUp(){
        super.levelUp(LEVELUP_STRENGTH, LEVELUP_DEXTERITY, LEVELUP_INTELLIGENCE);
    }

    /**
     * Calculates the total dps based on weapon and primary attribute (dexterity).
     */
    public void calculateDps() {
        super.calculateDps(this.getDexterity());
    }
}
