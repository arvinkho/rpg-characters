package no.noroff.accelerate.characters;


import no.noroff.accelerate.items.ArmorType;
import no.noroff.accelerate.items.WeaponType;

/**
 * Represents a mage. This is an extension of the character class.
 *
 * @author arvinkho
 */
public class Mage extends Character{

    private static final WeaponType[] WEAPON_TYPES = {WeaponType.STAFF, WeaponType.WAND};
    private static final ArmorType[] ARMOR_TYPES = {ArmorType.CLOTH};
    private static final int START_STRENGTH = 1;
    private static final int START_DEXTERITY = 1;
    private static final int START_INTELLIGENCE = 8;
    private static final int LEVELUP_STRENGTH = 1;
    private static final int LEVELUP_DEXTERITY = 1;
    private static final int LEVELUP_INTELLIGENCE = 5;

    /**
     * Creates an instance of a mage
     * @param name name of the mage
     */
    public Mage(String name){
        super(name, START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE, WEAPON_TYPES, ARMOR_TYPES);
        calculateDps();
    }


    /**
     * Levels up the mage. Increases the level by one point, and supplies with
     * mage specific values for increasing the base attributes.
     */
    public void levelUp(){
        super.levelUp(LEVELUP_STRENGTH, LEVELUP_DEXTERITY, LEVELUP_INTELLIGENCE);
    }

    /**
     * Calculates the total dps based on weapon and primary attribute (intelligence).
     */
    public void calculateDps() {
        super.calculateDps(this.getIntelligence());
    }
}
