
import no.noroff.accelerate.characters.PrimaryAttribute;
import no.noroff.accelerate.characters.Warrior;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.items.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ItemTest {

    @Test
    void equipItem_weaponLevelTooHigh_shouldThrowInvalidWeaponExceptionWithProperMessage() {
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Weapon weapon = new Weapon("Axe", 2, WeaponType.AXE, 10, 1);
        String expected = "Insufficient level!";
        // ACT
        Exception exception = assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipItem(weapon);
        });
        String actual = exception.getMessage();
        // ASSERT
        assertEquals(expected, actual);

    }

    @Test
    void equipItem_armorLevelTooHigh_shouldThrowInvalidArmorExceptionWithProperMessage(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Armor plateArmor = new Armor("Plate-body", 2, ArmorType.PLATE, Slot.BODY,
                new PrimaryAttribute(1, 1, 1));
        String expected = "Insufficient level!";
        // ACT
        Exception exception = assertThrows(InvalidArmorException.class, () -> {
            warrior.equipItem(plateArmor);
        });
        String actual = exception.getMessage();
        // ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void equipItem_incorrectWeaponType_shouldThrowInvalidWeaponExceptionWithProperMessage(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Weapon weapon = new Weapon("Bow", 1, WeaponType.BOW, 10, 1);
        String expected = "You can't equip this weapon type!";
        // ACT
        Exception exception = assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipItem(weapon);
        });
        String actual = exception.getMessage();
        // ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void equipItem_incorrectArmorType_shouldThrowInvalidArmorExceptionWithProperMessage(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Armor clothArmor = new Armor("Cloth armor", 1, ArmorType.CLOTH, Slot.BODY,
                new PrimaryAttribute(1, 1, 1));
        String expected = "You can't equip this armor type!";
        // ACT
        Exception exception = assertThrows(InvalidArmorException.class, () -> {
            warrior.equipItem(clothArmor);
        });
        String actual = exception.getMessage();
        // ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void equipItem_weaponSuccessful_shouldReturnTrue(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Weapon weapon = new Weapon("Axe", 1, WeaponType.AXE, 10, 1);
        // ACT
        boolean weaponEquipped = false;
        try {
            weaponEquipped = warrior.equipItem(weapon);
        } catch (InvalidWeaponException e) {
            fail();
        }
        // ASSERT
        assertTrue(weaponEquipped);
    }

    @Test
    void equipItem_armorSuccessful_shouldReturnTrue(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Armor plateArmor = new Armor("Plate-body", 1, ArmorType.PLATE, Slot.BODY,
                new PrimaryAttribute(1, 1, 1));
        boolean weaponEquipped = false;
        // ACT
        try {
            weaponEquipped = warrior.equipItem(plateArmor);
        } catch (InvalidArmorException e) {
            fail();
        }
        // ASSERT
        assertTrue(weaponEquipped);
    }

    @Test
    void getDps_noWeapon_shouldReturnCorrectDps(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        warrior.calculateDps();
        double expected = 1*(1 + (5.0/100));
        // ACT
        double actual = warrior.getDps();
        // ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void getDps_validWeapon_shouldReturnCorrectDps(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Weapon weapon = new Weapon("Axe", 1, WeaponType.AXE, 7, 1.1);
        double expectedDps = (7*1.1)*(1 + (5.0/100.0));
        // ACT
        try {
            warrior.equipItem(weapon);
        } catch (InvalidWeaponException e){
            fail();
        }
        warrior.calculateDps();
        double actualDps = warrior.getDps();
        // ASSERT
        assertEquals(expectedDps, actualDps);
    }

    @Test
    void getDps_validWeaponAndArmor_shouldReturnCorrectDps(){
        // ARRANGE
        Warrior warrior = new Warrior("Test-Warrior");
        Weapon weapon = new Weapon("Axe", 1, WeaponType.AXE, 7, 1.1);
        Armor plateArmor = new Armor("Plate-body", 1, ArmorType.PLATE, Slot.BODY,
                new PrimaryAttribute(1, 0, 0));
        double expectedDps = (7*1.1)*(1 + ((5.0+1.0)/100.0));
        // ACT
        try {
            warrior.equipItem(weapon);
            warrior.equipItem(plateArmor);
        } catch (InvalidWeaponException e){
            fail();
        } catch (InvalidArmorException e){
            fail();
        }
        warrior.calculateDps();
        double actualDps = warrior.getDps();
        // ASSERT
        assertEquals(expectedDps, actualDps);
    }
}
