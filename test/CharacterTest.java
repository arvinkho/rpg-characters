import no.noroff.accelerate.characters.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CharacterTest {

    @Test
    void getLevel_atInit_shouldReturnOne() {
        // ARRANGE
        Mage mage = new Mage("Test-Character");
        int expected = 1;
        // ACT
        int actual = mage.getLevel();
        // ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void getLevel_afterOneLevelUp_shouldReturnTwo() {
        // ARRANGE
        Mage mage = new Mage("Test-Character");
        int expected = 2;
        mage.levelUp();
        // ACT
        int actual = mage.getLevel();
        // ASSERT
        assertEquals(expected, actual);
    }

    @Test
    void getBaseAttributes_mageAtLevelOne_shouldReturnCorrectAttributes() {
        // ARRANGE
        Mage mage = new Mage("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(1, 1, 8);
        // ACT
        PrimaryAttribute actual = mage.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void getBaseAttributes_rangerAtLevelOne_shouldReturnCorrectAttributes() {
        // ARRANGE
        Ranger ranger = new Ranger("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(1, 7, 1);
        // ACT
        PrimaryAttribute actual = ranger.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void getBaseAttributes_rogueAtLevelOne_shouldReturnCorrectAttributes() {
        // ARRANGE
        Rogue rogue = new Rogue("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(2, 6, 1);
        // ACT
        PrimaryAttribute actual = rogue.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void getBaseAttributes_warriorAtLevelOne_shouldReturnCorrectAttributes() {
        // ARRANGE
        Warrior warrior = new Warrior("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(5, 2, 1);
        // ACT
        PrimaryAttribute actual = warrior.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void getBaseAttributes_mageAtLevelTwo_shouldReturnCorrectAttributes() {
        // ARRANGE
        Mage mage = new Mage("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(2, 2, 13);
        mage.levelUp();
        // ACT
        PrimaryAttribute actual = mage.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void getBaseAttributes_rangerAtLevelTwo_shouldReturnCorrectAttributes(){
        // ARRANGE
        Ranger ranger = new Ranger("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(2, 12, 2);
        ranger.levelUp();
        // ACT
        PrimaryAttribute actual = ranger.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void getBaseAttributes_rogueAtLevelTwo_shouldReturnCorrectAttributes(){
        // ARRANGE
        Rogue rogue = new Rogue("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(3, 10, 2);
        rogue.levelUp();
        // ACT
        PrimaryAttribute actual = rogue.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void getBaseAttributes_warriorAtLevelTwo_shouldReturnCorrectAttributes() {
        // ARRANGE
        Warrior warrior = new Warrior("Test-Character");
        PrimaryAttribute expected = new PrimaryAttribute(8, 4, 2);
        warrior.levelUp();
        // ACT
        PrimaryAttribute actual = warrior.getBaseAttributes();
        // ASSERT
        assertEquals(expected.getStrength(), actual.getStrength());
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }
}
